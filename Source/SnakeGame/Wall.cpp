// Fill out your copyright notice in the Description page of Project Settings.


#include "Wall.h"
#include "Kismet/GameplayStatics.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SnakeGameGameModeBase.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AWall::AWall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));

}

// Called when the game starts or when spawned
void AWall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWall::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid (Snake))
		{
			if (Snake->ReversActive)
			{
				Snake->Reversal = true;
			}
			else
			{
				/*TArray<AActor*> FoundActors;
				UGameplayStatics::GetAllActorsOfClass(GetWorld(), ASnakeElementBase::StaticClass(), FoundActors);

				for (AActor* ActorFound : FoundActors)
				{
					ActorFound->Destroy();
				}*/
				ASnakeGameGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
				if(GameMode)
				{
					GameMode->WriteSaveGame();
				}
				
				Snake->Destroy();	
			}
		
		}
	}
}

