// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "WallBlock.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API AWallBlock : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWallBlock();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		float MinDestroyDelay;

	UPROPERTY(EditDefaultsOnly)
		float MaxDestroyDelay;

	UPROPERTY(BlueprintReadWrite)
		bool Dead;

	UPROPERTY()
		float DestroyDelay;

	UPROPERTY(BlueprintReadWrite)
	bool DeathCome;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
