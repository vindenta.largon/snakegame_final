// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "DrawDebugHelpers.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
#include "Math/UnrealMathUtility.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DropChance = 5;
	DeadNow = false;
	AddFood = true;
	BornDelay = 2.f;
	MovingFail = false;
	NotEat = false;
	TimeRemove = 10.f;
	SnakeElementSize = 60;
	FoodGo = false;
		
	for(int32 i = 420; i >= -455; i = i - SnakeElementSize)
	{
		FieldCoordinatesX.Add(i);
	}

	for(int32 i = 660; i >= -690; i = i - SnakeElementSize)
	{
		FieldCoordinatesY.Add(i);
	}
	
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	TimerOn();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	if(FoodGo)
    	{
    		FoodGo = false;
    		FoodMoving();
    	}
}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			if(GetWorldTimerManager().IsTimerActive(TimerNotEat))
			{
				GetWorldTimerManager().ClearTimer(TimerNotEat);
			}
			
			TimerBorn();
			TimerOn();

			Snake->AddSnakeElement();
			Snake->HungerBar = 0.f;
			Snake->Scoring(1);

			int32 DropBonus = FMath::RandRange(1, 4);
			int32 ChanceChack = FMath::RandRange(0, 100);

				switch (DropBonus)
				{
				case 1:
					if (DropChance >= ChanceChack)
					{
						Snake->AddSpeedBonus();
					}
					break;
				case 2:
					if (DropChance >= ChanceChack)
					{
						Snake->AddSlowdownBonus();
					}
					break;
				case 3:
					if (DropChance >= ChanceChack)
					{
						Snake->AddSnakeReductionBonus();
					}
					break;
				case 4:
					if (DropChance >= ChanceChack)
					{
						Snake->AddReversBonus();
					}
					break;
				}
		}
	}

}

void AFood::FoodMoving()
{
	const FVector NewLoc = FoodRandomPos();
	SetActorLocation(NewLoc);
	AddFood = true;
	TimerOn();
	
}

FVector AFood::FoodRandomPos()
{
	FVector FoodLocation;

	bool BlockPos = false;

	int32 Count = 0;

	do
	{
		
		float PositionX = FieldCoordinatesX[FMath::FRandRange(0, FieldCoordinatesX.Num()-1)];
		float PositionY = FieldCoordinatesY[FMath::FRandRange(0, FieldCoordinatesY.Num()-1)];
		FoodLocation = FVector(PositionX, PositionY, 0);

		FHitResult HitResult;
		FVector TraceStart = FoodLocation - FVector(0.f, 0.f, 30.f);
		FVector TraceEnd = FoodLocation + FVector(0.f, 0.f, 150.f);

		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECC_Visibility);
		//DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Green, false, 3.f, 0, 12.f);

		if (const auto SnakeActor = Cast<ASnakeElementBase>(HitResult.GetActor()))
		{
			//DrawDebugSphere(GetWorld(), HitResult.GetActor()->GetActorLocation(), 13.f, 24, FColor::Red, false, 3.f, 0, 12.f);
			BlockPos = true;
		}

		BlockPos = HitResult.bBlockingHit;

		Count++;

	}while (BlockPos && Count < 20);
	
	if (BlockPos)
	{
		MovingFail = true;
	}

	return FoodLocation;

}

void AFood::TimerOn()
{
	//if(GetWorldTimerManager().IsTimerActive(TimerNotEat))
	//{
		GetWorldTimerManager().ClearTimer(TimerNotEat);
	//}
	FTimerDelegate DelegateMove;
	DelegateMove.BindLambda([this] {TimerBorn(); });
	GetWorldTimerManager().SetTimer(TimerNotEat, DelegateMove, TimeRemove, false);

	//if(GetWorldTimerManager().IsTimerActive(TimerSignalEat))
	//{
		GetWorldTimerManager().ClearTimer(TimerSignalEat);
	//}
	FTimerDelegate DelegateSignal;
	DelegateSignal.BindLambda([this] {NotEat = true; });
	GetWorldTimerManager().SetTimer(TimerSignalEat, DelegateSignal, TimeRemove - 4.f, false);
}

void AFood::TimerBorn()
{
	//if(GetWorldTimerManager().IsTimerActive(TimerNotEat))
	//{
	//	GetWorldTimerManager().ClearTimer(TimerNotEat);
	//}
	DeadNow = true;
	NotEat = false;

	//GetWorldTimerManager().ClearTimer(TimerDestroyEat);
	//FTimerDelegate DelegateDest;
	//DelegateDest.BindLambda([this] {FoodMoving(); });
	//GetWorldTimerManager().SetTimer(TimerDestroyEat, DelegateDest, BornDelay, false);
}

