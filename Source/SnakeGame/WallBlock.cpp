// Fill out your copyright notice in the Description page of Project Settings.


#include "WallBlock.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SnakeGameGameModeBase.h"
#include "Kismet/GameplayStatics.h"
#include "Math/UnrealMathUtility.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
AWallBlock::AWallBlock()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	Dead = false;
	DeathCome = false;
	MinDestroyDelay = 5.f;
	MaxDestroyDelay = 10.f;
	
}

// Called when the game starts or when spawned
void AWallBlock::BeginPlay()
{
	Super::BeginPlay();

	DestroyDelay = FMath::FRandRange(MinDestroyDelay, MaxDestroyDelay);
	FTimerHandle TimerDead = FTimerHandle();
	FTimerDelegate DelegateDead;
	DelegateDead.BindLambda([this] {Dead = true; DeathCome = false; });
	GetWorld()->GetTimerManager().SetTimer(TimerDead, DelegateDead, DestroyDelay, false);

	FTimerHandle TimerDeathCome = FTimerHandle();
	FTimerDelegate DelegateDeathCome;
	DelegateDeathCome.BindLambda([this] {DeathCome = true; });
	GetWorld()->GetTimerManager().SetTimer(TimerDeathCome, DelegateDeathCome, DestroyDelay-1.6, false);
}

// Called every frame
void AWallBlock::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWallBlock::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{

			if (Snake->ReversActive)
			{
				Snake->Reversal = true;
			}
			else
			{
				ASnakeGameGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>();
				if(GameMode)
				{
					GameMode->WriteSaveGame();
				}
				
				Snake->Destroy();
			}
		}

	}
}

