// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SnakeGameGameModeBase.h"
#include "SnakeBase.generated.h"


class ASnakeElementBase;
class AFood;
class ASlowdownBonus;
class ASpeedBonus;
class ASnakeReductionBonus;
class AReversBonus;
class AWallBlock;
class ASnakeGameGameModeBase;


UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT,
};


UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeElementBase> SnakeElementClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASlowdownBonus> SlowdownBonusClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASpeedBonus> SpeedBonusClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeReductionBonus> SnakeReductionBonusClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AReversBonus> ReversBonusClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AWallBlock> WallBlockClass;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASnakeGameGameModeBase> SnakeGMB;

	UPROPERTY()
		TArray<ASnakeElementBase*> SnakeElements;

	UPROPERTY()
	TArray<int32> FieldCoordinatesX;

	UPROPERTY()
	TArray<int32> FieldCoordinatesY;

	UPROPERTY(BlueprintReadOnly)
		bool RandomLocationFail;

	UPROPERTY(BlueprintReadOnly)
		bool Victory;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		int32 ElementMax;

	UPROPERTY(EditDefaultsOnly)
		float ElementSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float MovementSpeed;

	UPROPERTY(EditDefaultsOnly)
		int32 SnakeLength;

	UPROPERTY(BlueprintReadOnly)
		int32 Score;

	UPROPERTY(BlueprintReadOnly)
	int32 BestScore;

	UPROPERTY(BlueprintReadWrite)
		float HungerBar;

	UPROPERTY(BlueprintReadWrite)
		bool ReversActive;

	UPROPERTY()
		bool Reversal;

	UPROPERTY(EditDefaultsOnly)
		int32 BonusPoints;

	UPROPERTY(BlueprintReadWrite)
		bool WallBlockSpawn;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float MinWallBlockTime;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float MaxWallBlockTime;

	UPROPERTY()
		float WallBlockTime;
	
	UPROPERTY()
	FTimerHandle TimerSpawnWall = FTimerHandle();

	UPROPERTY()
	FTimerHandle TimerFirstFoodSpawn = FTimerHandle();



	UPROPERTY()
		ASnakeElementBase* CurrentElem;
	UPROPERTY()
		ASnakeElementBase* PrevElem;
	UPROPERTY()
		FVector PrevLoc;
	UPROPERTY()
		float CurrentX;
	UPROPERTY()
		float PrevX;
	UPROPERTY()
		float CurrentY;
	UPROPERTY()
		float PrevY;
	UPROPERTY(BlueprintReadWrite)
	float ReversLifeDelay;

	UPROPERTY(BlueprintReadWrite)
		int CurrentLevel;

	UPROPERTY()
		EMovementDirection MovementCurrent;

	UPROPERTY()
		EMovementDirection LastMoveDirection;

	UPROPERTY()
		ASnakeElementBase* SnakeElementRemove;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
		void AddSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void RemoveSnakeElement(int ElementsNum = 1);

	UFUNCTION(BlueprintCallable)
		void Move();

	UFUNCTION()
		void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);

	UFUNCTION(BlueprintCallable)
		void AddSlowdownBonus();

	UFUNCTION(BlueprintCallable)
		void AddSpeedBonus();

	UFUNCTION(BlueprintCallable)
		void AddSnakeReductionBonus();

	UFUNCTION(BlueprintCallable)
		void AddReversBonus();

	UFUNCTION(BlueprintCallable)
		void SpeedChange(float Speed, float Delay, bool Speedup);

	UFUNCTION(BlueprintCallable)
		void ReversMove(float ReversDelay);

	UFUNCTION(BlueprintCallable)
		void AddWallBlock(int BlockNum = 1);

	UFUNCTION()
		void SpeedChangeCompleted();

	UFUNCTION()
		FTransform RandomLocation(int32 WallBlockNum = 1);

	UFUNCTION()
		void Scoring(int32 PointSign = 1);



};
