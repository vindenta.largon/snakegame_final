// Fill out your copyright notice in the Description page of Project Settings.


#include "SPlayerState.h"

int32 ASPlayerState::GetCredits() const
{
	return PlayScore;
}

int32 ASPlayerState::GetBestResult() const
{
	return BestScore;
}

void ASPlayerState::AddScore(int32 Delta)
{
	PlayScore += Delta;
}

void ASPlayerState::LoadPlayerState_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		BestScore = SaveObject->BestResult;
	}
}

void ASPlayerState::SavePlayerState_Implementation(USSaveGame* SaveObject)
{
	if(SaveObject)
	{
		if(PlayScore > BestScore)
		{
			SaveObject->BestResult = PlayScore;
			BestScore = PlayScore;
		}
		else
		{
			SaveObject->BestResult = BestScore;
		}
	}
}