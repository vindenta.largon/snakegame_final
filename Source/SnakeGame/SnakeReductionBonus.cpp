// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeReductionBonus.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASnakeReductionBonus::ASnakeReductionBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DestroyDelay = 15.f;
	Death = false;
}



// Called when the game starts or when spawned
void ASnakeReductionBonus::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle TimerDestroy = FTimerHandle();
	FTimerDelegate DelegateDestroy;
	DelegateDestroy.BindLambda([this] {Death = true; });
	GetWorld()->GetTimerManager().SetTimer(TimerDestroy, DelegateDestroy, DestroyDelay, false);	

}

// Called every frame
void ASnakeReductionBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASnakeReductionBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid(Snake))
		{
			if(Death == false)
			{
				Snake->Scoring(-1);
				Snake->RemoveSnakeElement();
				Death = true;
			}
		}
	}
	else
	{
		Death = true;
	}
}