// Fill out your copyright notice in the Description page of Project Settings.


#include "SlowdownBonus.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASlowdownBonus::ASlowdownBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DestroyDelay = 15.f;
	BonusSpeed = 1.f;
	SpeedChangeDelay = 15.f;
	Death = false;
}



// Called when the game starts or when spawned
void ASlowdownBonus::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle TimerDestroy = FTimerHandle();
	FTimerDelegate DelegateDestroy;
	DelegateDestroy.BindLambda([this] {Death = true; });
	GetWorld()->GetTimerManager().SetTimer(TimerDestroy, DelegateDestroy, DestroyDelay, false);
}

// Called every frame
void ASlowdownBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASlowdownBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		
		if (IsValid (Snake))
		{
			if(Death == false)
			{
				Snake->Scoring(-2);
				Snake->SpeedChange(BonusSpeed, SpeedChangeDelay, false);
				Death = true;
			}
		}
	}
	else
	{
		Destroy();
	}
}


