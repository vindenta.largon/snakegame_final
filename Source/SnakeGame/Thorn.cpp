// Fill out your copyright notice in the Description page of Project Settings.


#include "Thorn.h"

// Sets default values
AThorn::AThorn()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AThorn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AThorn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

