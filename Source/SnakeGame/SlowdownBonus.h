// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "SlowdownBonus.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API ASlowdownBonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASlowdownBonus();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<ASlowdownBonus> SlowdownBonusClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float DestroyDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float BonusSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float SpeedChangeDelay;

	UPROPERTY(BlueprintReadWrite)
		bool Death;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;


};
