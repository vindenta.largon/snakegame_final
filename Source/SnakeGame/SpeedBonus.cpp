// Fill out your copyright notice in the Description page of Project Settings.


#include "SpeedBonus.h"
#include "SnakeBase.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"

// Sets default values
ASpeedBonus::ASpeedBonus()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	DestroyDelay = 15.f;
	BonusSpeed = 0.25;
	SpeedChangeDelay = 15.f;
	Death = false;
	
	
}


// Called when the game starts or when spawned
void ASpeedBonus::BeginPlay()
{
	Super::BeginPlay();

	FTimerHandle TimerDestroy = FTimerHandle();
	FTimerDelegate DelegateDestroy;
	DelegateDestroy.BindLambda([this] {Death = true;});
	GetWorld()->GetTimerManager().SetTimer(TimerDestroy, DelegateDestroy, DestroyDelay, false);
}

// Called every frame
void ASpeedBonus::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ASpeedBonus::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);

		if (IsValid(Snake))
		{
			if(Death == false)
			{
				MeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
				Snake->Scoring(2);
				Snake->SpeedChange(BonusSpeed, SpeedChangeDelay, true);
				Death = true;
			}
		}
	}
	else
	{
		Death = true;
	}
}

