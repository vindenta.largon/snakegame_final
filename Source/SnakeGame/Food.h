// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "Food.generated.h"

class UStaticMeshComponent;



UCLASS()
class SNAKEGAME_API AFood : public AActor, public IInteractable 
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFood();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AFood> FoodClass;

	UPROPERTY()
	TArray<int32> FieldCoordinatesX;

	UPROPERTY()
	TArray<int32> FieldCoordinatesY;

	UPROPERTY(EditDefaultsOnly)
	int32 SnakeElementSize;

	UPROPERTY(EditDefaultsOnly)
		int32 DropChance;

	UPROPERTY(BlueprintReadWrite)
		bool DeadNow;

	UPROPERTY(BlueprintReadWrite)
		bool AddFood;

	UPROPERTY(BlueprintReadWrite)
		bool NotEat;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float BornDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float TimeRemove;

	UPROPERTY(BlueprintReadWrite)
	bool FoodGo;
	
		FTimerHandle TimerNotEat = FTimerHandle();

		FTimerHandle TimerDestroyEat = FTimerHandle();

		FTimerHandle TimerSignalEat = FTimerHandle();

	UPROPERTY()
		bool MovingFail;

	UFUNCTION()
		void FoodMoving();

	UFUNCTION()
		FVector FoodRandomPos();

	UFUNCTION()
		void TimerOn();

	UFUNCTION()
		void TimerBorn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;




};
