// Copyright Epic Games, Inc. All Rights Reserved.


#include "SnakeGameGameModeBase.h"

#include "SPlayerState.h"
#include "SSaveGame.h"
#include "GameFramework/GameStateBase.h"
#include "Kismet/GameplayStatics.h"

ASnakeGameGameModeBase::ASnakeGameGameModeBase()
{
	SlotName = "BestResult";
}

void ASnakeGameGameModeBase::InitGame(const FString& MapName, const FString& Options, FString& ErrorMessage)
{
	Super::InitGame(MapName, Options, ErrorMessage);

	LoadSaveGame();
}

void ASnakeGameGameModeBase::HandleStartingNewPlayer_Implementation(APlayerController* NewPlayer)
{
	Super::HandleStartingNewPlayer_Implementation(NewPlayer);

	ASPlayerState* PS = NewPlayer->GetPlayerState<ASPlayerState>();

	if(PS)
	{
		PS->LoadPlayerState(CurrentSaveGame);
	}
}

void ASnakeGameGameModeBase::LoadSaveGame()
{
	if (UGameplayStatics::DoesSaveGameExist(SlotName,0))
	{
		CurrentSaveGame = Cast<USSaveGame>(UGameplayStatics::LoadGameFromSlot(SlotName,0));

		if(CurrentSaveGame == nullptr)
		{
			UE_LOG(LogTemp,Warning,TEXT("Filed To Load SaveGame Data."))
			return;
		}
		UE_LOG(LogTemp,Warning,TEXT("Loaded SaveGame Data."))
	}
	else
	{
		CurrentSaveGame = Cast<USSaveGame>(UGameplayStatics::CreateSaveGameObject(USSaveGame::StaticClass()));

		UGameplayStatics::SaveGameToSlot(CurrentSaveGame,SlotName,0);

		CurrentSaveGame->BestResult = 0;

		UE_LOG(LogTemp,Warning,TEXT("Create new SaveGame Data."))
	}
}

void ASnakeGameGameModeBase::WriteSaveGame()
{
	ASPlayerState* PS  = Cast<ASPlayerState>(GameState->PlayerArray[0]);
	
	if(PS)
	{
		PS->SavePlayerState(CurrentSaveGame);
	}
	
	UGameplayStatics::SaveGameToSlot(CurrentSaveGame, SlotName, 0);
}
