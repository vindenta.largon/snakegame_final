// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "SlowdownBonus.h"
#include "SpeedBonus.h"
#include "SnakeReductionBonus.h"
#include "ReversBonus.h"
#include "Food.h"
#include "WallBlock.h"
#include "DrawDebugHelpers.h"
#include "SnakeGameGameModeBase.h"
#include "SPlayerState.h"
#include "SSaveGame.h"
#include "Algo/Reverse.h"
#include "GameFramework/GameStateBase.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 60.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::DOWN;
	Reversal = false;
	ReversActive = false;
	CurrentLevel = 1;
	HungerBar = 0.f;
	Score = 0;
	BonusPoints = 5;
	WallBlockSpawn = true;
	ElementMax = 345;
	MinWallBlockTime = 5.f;
	MaxWallBlockTime = 45.f;
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();

	for(int32 i = 420; i >= -455; i = i - ElementSize)
	{
		FieldCoordinatesX.Add(i);
	}

	for(int32 i = 660; i >= -690; i = i - ElementSize)
	{
		FieldCoordinatesY.Add(i);
	}
	
	WallBlockSpawn = true;
	SetActorTickInterval(MovementSpeed);
	SnakeLength = 5;
	AddSnakeElement(SnakeLength);

	/*FTimerDelegate DelegateFirstFoodSpawn;
	DelegateFirstFoodSpawn.BindLambda([this]{AFood* FoodNew = GetWorld()->SpawnActor<AFood>(FoodClass, RandomLocation()); });
	GetWorld()->GetTimerManager().SetTimer(TimerFirstFoodSpawn, DelegateFirstFoodSpawn, 2.f, false);
	*/
	AFood* FoodNew = GetWorld()->SpawnActor<AFood>(FoodClass, RandomLocation()); 

	ASPlayerState* PS = Cast<ASPlayerState>(GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>()->GameState->PlayerArray[0]);
	if(PS)
	{
		BestScore = PS->GetBestResult();
	}

	
}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move();
	MovementCurrent = LastMoveDirection;

	if (SnakeElements.Num() < ElementMax-10)
	{
		if (WallBlockSpawn)
		{
			WallBlockSpawn = false;
			WallBlockTime = FMath::FRandRange(MinWallBlockTime, MaxWallBlockTime);
			FTimerDelegate DelegateSpawnWall;
			DelegateSpawnWall.BindLambda([this] {AddWallBlock(FMath::RandRange(1, 3)); });
			GetWorld()->GetTimerManager().SetTimer(TimerSpawnWall, DelegateSpawnWall, WallBlockTime, false);
		}
	}

}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform = FTransform(NewLocation);
		if (SnakeElements.Num() > SnakeLength-1)
		{
			int32 LastElemIndex = SnakeElements.Num() - 1;
			NewLocation = SnakeElements[LastElemIndex]->GetActorLocation();
			NewTransform = FTransform(NewLocation);
		}
		
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();			
		}
	}
}

void ASnakeBase::RemoveSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		int32 LastElemIndex = SnakeElements.Num() - 1;

		if (LastElemIndex > 2)
		{
			SnakeElementRemove = SnakeElements[LastElemIndex];
			SnakeElements.RemoveAt(LastElemIndex);
			SnakeElementRemove->DeadDissolve();
			
			--SnakeLength;
		}

	}
}

void ASnakeBase::Move()
{
	
	FVector MovementVector(FVector::ZeroVector);

	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += ElementSize;
		break;

	case EMovementDirection::DOWN:
		MovementVector.X -= ElementSize;
		break;
	
	case EMovementDirection::LEFT:
		MovementVector.Y += ElementSize;
		break;
	
	case EMovementDirection::RIGHT:
		MovementVector.Y -= ElementSize;
		break;
	}

	SnakeElements[0]->ToggleCollision();

	for (int i = SnakeElements.Num() - 1; i > 0; i--)
	{
		auto CurrentElement = SnakeElements[i];
		auto PrevElement = SnakeElements[i - 1];
		FVector PrevLocation = PrevElement->GetActorLocation();
		CurrentElement->SetActorLocation(PrevLocation);
	}
	
	if (ReversActive, Reversal)
	{
		for (int j = SnakeElements.Num() - 1, i = 1; i < j; ++i, --j)
		{
			ASnakeElementBase* SnakeElem = SnakeElements[i];
			SnakeElements[i] = SnakeElements[j];
			SnakeElements[j] = SnakeElem;
		}
				
		if (CurrentX > PrevX)
		{
			LastMoveDirection = EMovementDirection::UP;
			PrevLoc.X += ElementSize;
			PrevLoc.Z = 0;
		}
		else if (CurrentX < PrevX)
		{
			LastMoveDirection = EMovementDirection::DOWN;
			PrevLoc.X -= ElementSize;
			PrevLoc.Z = 0;
		}
		else if (CurrentY > PrevY)
		{
			LastMoveDirection = EMovementDirection::LEFT;
			PrevLoc.Y += ElementSize;
			PrevLoc.Z = 0;
		}
		else if (CurrentY < PrevY)
		{
			LastMoveDirection = EMovementDirection::RIGHT;
			PrevLoc.Y -= ElementSize;
			PrevLoc.Z = 0;
		}

		SnakeElements[0]->SetActorLocation(PrevLoc);
		Reversal = false;
	}
	else
	{
		SnakeElements[0]->AddActorWorldOffset(MovementVector);
	}

	int iElem = SnakeElements.Num() - 1;
	CurrentElem = SnakeElements[iElem];
	PrevElem = SnakeElements[iElem - 1];
	PrevLoc = PrevElem->GetActorLocation();
	CurrentX = CurrentElem->GetActorLocation().X;
	PrevX = PrevElem->GetActorLocation().X;
	CurrentY = CurrentElem->GetActorLocation().Y;
	PrevY = PrevElem->GetActorLocation().Y;
	
	SnakeElements[0]->ToggleCollision();
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int ElementIndex;
		SnakeElements.Find(OverlappedElement, ElementIndex);
		bool bIsFirst = ElementIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::AddSlowdownBonus()
{
	ASlowdownBonus* NewSlowdownBonus = GetWorld()->SpawnActor<ASlowdownBonus>(SlowdownBonusClass, RandomLocation());
}

void ASnakeBase::AddSpeedBonus()
{
	ASpeedBonus* NewSpeedBonus = GetWorld()->SpawnActor<ASpeedBonus>(SpeedBonusClass, RandomLocation());
}

void ASnakeBase::AddSnakeReductionBonus()
{
	ASnakeReductionBonus* NewSnakeReductionBonus = GetWorld()->SpawnActor<ASnakeReductionBonus>(SnakeReductionBonusClass, RandomLocation());
}

void ASnakeBase::AddReversBonus()
{
	AReversBonus* NewReversBonus = GetWorld()->SpawnActor<AReversBonus>(ReversBonusClass, RandomLocation());
}

void ASnakeBase::SpeedChange(float Speed, float Delay, bool Speedup)
{
	SetActorTickInterval(Speed);
	FTimerHandle TimerSpeedChange = FTimerHandle();

	if (Speedup)
	{
		SnakeElements[0]->SpeedBonusOff = true;
		SnakeElements[0]->SpeedBonusOn = true;
		SnakeElements[0]->SlowdownOn = false;
		FTimerDelegate DelegateSpeedChange;
		DelegateSpeedChange.BindLambda([this] {SpeedChangeCompleted(); });
		GetWorld()->GetTimerManager().SetTimer(TimerSpeedChange, DelegateSpeedChange, Delay, false);
	}
	else
	{
		SnakeElements[0]->SlowdownOff = true;
		SnakeElements[0]->SlowdownOn = true;
		SnakeElements[0]->SpeedBonusOn = false;
		FTimerDelegate DelegateSpeedChange;
		DelegateSpeedChange.BindLambda([this] {SpeedChangeCompleted(); });
		GetWorld()->GetTimerManager().SetTimer(TimerSpeedChange, DelegateSpeedChange, Delay, false);
	}
}

void ASnakeBase::ReversMove(float ReversDelay)
{
	ReversLifeDelay = ReversDelay;
	SnakeElements[0]->ReversOff = true;
	ReversActive = true;
	SnakeElements[0]->ReversOn = true;
	//FTimerHandle TimerRevers = FTimerHandle();
	//FTimerDelegate DelegateRevers;
	//DelegateRevers.BindLambda([this] {ReversActive = false; SnakeElements[0]->ReversOff = false; });
	//GetWorld()->GetTimerManager().SetTimer(TimerRevers, DelegateRevers, ReversDelay, false);
}

void ASnakeBase::AddWallBlock(int BlockNum)
{

	FTransform RandomTransform;
	bool BlockPos = false;
	int32 Count = 0;
	float PosX = 0;
	float PosY = 0;

	if (BlockNum == 1)
	{
		RandomTransform = RandomLocation(1);
	}

	if (BlockNum == 2)
	{
		RandomTransform = RandomLocation(2);
	}

	if (BlockNum == 3)
	{
		RandomTransform = RandomLocation(3);
	}

	for (int i = 0; i < BlockNum; i++)
	{
		if (i >= 1)
		{
				TArray<float> RandomArr;
				RandomArr.Add(-80.f);
				RandomArr.Add(0.f);
				RandomArr.Add(80.f);
				int32 RandX = FMath::RandRange(0, 2);
				int32 RandY = FMath::RandRange(0,2);
				float NewPosX = RandomArr[RandX];
				float NewPosY = RandomArr[RandY];
				FVector NewLocBlock = FVector(NewPosX, NewPosY, 0);
				RandomTransform = RandomTransform * FTransform(NewLocBlock);
		}

		AWallBlock* NewWallBlock = GetWorld()->SpawnActor<AWallBlock>(WallBlockClass, RandomTransform);

		if (IsValid(NewWallBlock))
		{
				float ResetTime = NewWallBlock->DestroyDelay;
				FTimerHandle TimerReset = FTimerHandle();
				FTimerDelegate DelegateReset;
				DelegateReset.BindLambda([this] {WallBlockSpawn = true; });
				GetWorld()->GetTimerManager().SetTimer(TimerReset, DelegateReset, ResetTime, false);
		}
	}
}

void ASnakeBase::SpeedChangeCompleted()
{
	SetActorTickInterval(MovementSpeed);
	SnakeElements[0]->SpeedBonusOn = false;
	SnakeElements[0]->SlowdownOn = false;
}

FTransform ASnakeBase::RandomLocation(int32 WallBlockNum)
{
	FTransform RandomTransform;
	bool BlockPos = false;
	int32 Count = 0;
	FHitResult HitResult;
	FVector TraceStart;
	FVector TraceEnd;

	do
	{
		float PosX;
		float PosY;
		switch (WallBlockNum)
		{
		case 1:
			PosX = FieldCoordinatesX[FMath::FRandRange(0, FieldCoordinatesX.Num()-1)];
			PosY = FieldCoordinatesY[FMath::FRandRange(0, FieldCoordinatesY.Num()-1)];
			break;
		case 2:
			PosX = FieldCoordinatesX[FMath::FRandRange(1, FieldCoordinatesX.Num()-2)];
			PosY = FieldCoordinatesY[FMath::FRandRange(1, FieldCoordinatesY.Num()-2)];
			break;
		case 3:
			PosX = FieldCoordinatesX[FMath::FRandRange(2, FieldCoordinatesX.Num()-3)];
			PosY = FieldCoordinatesY[FMath::FRandRange(2, FieldCoordinatesY.Num()-3)];
			break;
		default:
			PosX = FieldCoordinatesX[FMath::FRandRange(0, FieldCoordinatesX.Num()-1)];
			PosY = FieldCoordinatesY[FMath::FRandRange(0, FieldCoordinatesY.Num()-1)];
			break;
		}
		FVector RandomV = FVector(PosX, PosY, 0);
		RandomTransform = FTransform(RandomV);
		
		TraceStart = RandomV - FVector(0.f, 0.f, 30.f);
		TraceEnd = RandomV + FVector(0.f, 0.f, 150.f);
		GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECC_Visibility);

		if (const auto SnakeActor = Cast<ASnakeElementBase>(HitResult.GetActor()))
		{
			BlockPos = true;
		}

		BlockPos = HitResult.bBlockingHit;
		Count++;

	} while (BlockPos && Count < 10);

	if (BlockPos, SnakeElements.Num() > ElementMax - 20)
	{
		Victory = true;
	}
	else if (BlockPos, SnakeElements.Num() <= ElementMax - 20)
	{
		RandomLocationFail = true;
	}

	return RandomTransform;
}

void ASnakeBase::Scoring(int32 PointSign)
{
	int32 NewPoint = abs(BonusPoints)*PointSign;
	Score += NewPoint;

	ASPlayerState* PS = Cast<ASPlayerState>(GetWorld()->GetAuthGameMode<ASnakeGameGameModeBase>()->GameState->PlayerArray[0]);
	if(PS)
	{
		PS->AddScore(NewPoint);
	}
}



