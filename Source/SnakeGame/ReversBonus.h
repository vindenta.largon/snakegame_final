// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Interactable.h"
#include "ReversBonus.generated.h"

class UStaticMeshComponent;

UCLASS()
class SNAKEGAME_API AReversBonus : public AActor, public IInteractable
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AReversBonus();

	UPROPERTY(VisibleAnyWhere, BlueprintReadOnly)
		UStaticMeshComponent* MeshComponent;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AReversBonus> ReversBonusClass;

	UPROPERTY(EditDefaultsOnly)
	int32 SnakeElementSize;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float DestroyDelay;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
		float BonusDelay;

	UPROPERTY(BlueprintReadWrite)
		bool Death;



	
	
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact(AActor* Interactor, bool bIsHead) override;

};
