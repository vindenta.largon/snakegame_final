// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "SSaveGame.h"
#include "GameFramework/PlayerState.h"
#include "SPlayerState.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API ASPlayerState : public APlayerState
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Score")
	int32 PlayScore = 0;
	
	int32 BestScore;

public:

	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 GetCredits() const;
	
	UFUNCTION(BlueprintCallable, Category = "Score")
	int32 GetBestResult() const;

	UFUNCTION(BlueprintCallable, Category = "Score")
	void AddScore(int32 Delta);

	UFUNCTION(BlueprintNativeEvent)
	void LoadPlayerState(USSaveGame* SaveObject);

	UFUNCTION(BlueprintNativeEvent)
	void SavePlayerState(USSaveGame* SaveObject);

	
	
};
